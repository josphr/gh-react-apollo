import base64 from 'base-64'

const config = {
    GITHUB_CLIENT_ID : 'f3b7e303d93cfc4410b0',
    GITHUB_CLIENT_SECRET :'212a547706ce0eb407e5429dae1572b149e94da1' 
}

const AUTH_URL_PATH = 'https://api.github.com/authorizations'

export async function login (username,password)  {
    const bytes = username.trim() + ':' + password.trim()
    const accessToken = base64.encode(bytes)

    try {
        const res = await fetch(AUTH_URL_PATH, {
            method: 'POST',
            headers: {
              Authorization: 'Basic ' + accessToken,
              'User-Agent': 'GitHub Issue Browser',
              'Content-Type': 'application/json; charset=utf-8',
              Accept: 'application/vnd.github.inertia-preview+json'
            },
            body: JSON.stringify({
              client_id: config.GITHUB_CLIENT_ID,
              client_secret: config.GITHUB_CLIENT_SECRET,
              scopes: ['user', 'repo'],
              note: 'not abuse'
            })
        })
        return res.json()

    } catch (err) {
        console.log(err)
        return err
    }
}

