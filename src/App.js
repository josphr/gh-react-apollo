import React, { Component } from 'react'
import { ApolloProvider } from 'react-apollo'
import ApolloClient from 'apollo-client'
import { createHttpLink } from 'apollo-link-http'
import { setContext } from 'apollo-link-context'
import { InMemoryCache } from "apollo-cache-inmemory";
import { login } from './githubOauth'
import { username , password } from './config.default'

import Repository from './repository'


let TOKEN = null

const httpLink = createHttpLink({
  uri : 'https://api.github.com/graphql'
})

const middlewareLink = setContext(() => ({
  headers : {
    authorization : `Bearer ${TOKEN}`
    }
}))

const link = middlewareLink.concat(httpLink)

const client = new ApolloClient({
  link ,
  cache : new InMemoryCache()
})




class App extends Component {

  constructor () {
    super()
    this.state = { login: false }
  }

  async componentDidMount () {
    if (username === 'xxx') {
      throw new Error('Please create a config.js your username and password.')
    }
    const { token } = await login(username, password)
    TOKEN = token
    this.setState({
      login : true
    })
  }

  routeForRepository (login, name) {
    return {
      title: `${login}/${name}`,
      component: Repository,login,name
    }
  }

  render(){
    if (!this.state.login) {
      return <p>Login...</p>
    }

    return this.state.login ? 
      <ApolloProvider client={client}>
      <Repository {...this.routeForRepository('facebook','react')}/>
      </ApolloProvider>
      : <p>Logging in...</p>
  }
}

export default App
